const { Builder } = require('selenium-webdriver')
const environment = process.env.ENV

let driver

const remoteHub = 'http://chrome:4444/wd/hub'

before(async() => {
  const server = environment === 'local' ? '' : remoteHub

  driver = await new Builder().forBrowser('chrome').usingServer(server).build()
  driver.manage().window().maximize()

  exports.driver = driver
})

describe('Test Suite', () => {
  require('./tests/amazonTest')
  require('./tests/skyscannerTest')
  require('./tests/gmailTest')
})

after(() => driver.quit())
