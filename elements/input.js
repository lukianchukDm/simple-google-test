const { Key } = require('selenium-webdriver')

class Input {
  constructor(driver, selector) {
    this.driver   = driver
    this.selector = selector
  }

  get input() {
    return this.driver.findElement(this.selector)
  }

  async fill(text) {
    await this.input.sendKeys(text)
  }

  async pressEnter() {
    await this.input.sendKeys(Key.RETURN)
  }
}

module.exports = Input
