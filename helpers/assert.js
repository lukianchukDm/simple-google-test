const assert = require('assert')

class Assert {
  constructor(driver) {
    this.driver = driver
  }

  async equals(value1, value2) {
    try {
      await assert.equal(value1, value2)
    } catch (error) {
      await this._takeScreenshot()
      throw error
    }
  }

  async isTrue(statement) {
    await this.equals(statement, true)
  }

  async isFalse(statement) {
    await this.equals(statement, false)
  }

  async deepEquals(obj1, obj2) {
    for (let i = 0; i < obj1.length; i++) {
      await this.equals(obj1[i], obj2[i])
    }
  }

  async _takeScreenshot() {
    const totalHeight  = await this.driver.executeScript('return document.body.offsetHeight')
    const windowHeight = await this.driver.executeScript('return window.outerHeight')
    const scroll = windowHeight - 300

    let position = 0
    for (let i = 0; i < totalHeight / scroll; i++) {
      await this.driver.executeScript(`window.scrollTo(0, ${position})`)

      const image = await this.driver.takeScreenshot()
      allure.createAttachment(`Screenshot ${i}`,() => new Buffer(image, 'base64'), 'image/png')()

      position + windowHeight > totalHeight ? i = 9 : position = position + scroll
    }
  }
}

module.exports = Assert
