const waitForElement = async(driver, selector) => {
  for (let i = 0; i < 10; i++) {
    try {
      await driver.wait(() => driver.findElement(selector).isDisplayed(), 5000)
    } catch (e) {
      e.name === 'NoSuchElementError' ? await driver.sleep(1000) : 'ok'
    }
  }
}

module.exports = waitForElement
