FROM node:12-buster-slim

ENV HOME /src
USER root
RUN mkdir -p $HOME
WORKDIR /src

COPY . $HOME/
RUN npm install --no-optional
RUN npm install mocha-allure-reporter

ENTRYPOINT npm run test-remote-docker