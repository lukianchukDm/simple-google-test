const Assert = require('../helpers/assert')
const Page   = require('../pageObjects/page')

const google     = 'https://www.google.com/?gl=us'
const searchText = 'skyscanner'

describe('Searching "skyscanner" in google', () => {
  let driver
  let page
  let assert

  beforeEach(async() => {
    driver = require('../index').driver
    page   = new Page(driver)
    assert = new Assert(driver)

    await page.navigateTo(google)
  })

  it('Should count the target links', async() => {
    const googleSearch  = page.googleHome
    const googleResults = page.googleResults

    await googleSearch.fillInput(searchText)
    await googleSearch.clickSearchButton()

    const results = await googleResults.getSearchResults()
      .then(async(results) => await googleResults.filterResults(results))

    let count = 0
    for (const result of results) {
      result.toLowerCase().includes(searchText) ? count++ : count
    }

    await assert.isTrue(count > 13)
  })
})
