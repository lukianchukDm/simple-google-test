const Assert = require('../helpers/assert')
const Page   = require('../pageObjects/page')
const gmail  = 'https://bit.ly/2pdCLIl'

describe('Test Gmail', () => {
  let driver
  let page
  let assert

  before(async() => {
    driver = require('../index').driver
    page   = new Page(driver)
    assert = new Assert(driver)
  })

  beforeEach(async() => {
    await page.navigateTo(gmail)

  })

  it('Should send email', async() => {
    const user = {
      login:    'automationTest322@gmail.com',
      password: 'aasfm477fhjJSF32'
    }
    const mail = {
      recipient: 'dmitriyl@slatestudio.com',
      subject:   'Test message',
      message:   'This is a test message and it is written by autotest!'
    }

    const gmail = page.gmail

    await page.gmailLogin.signIn(user)
    await gmail.sendEmail(mail)

    await gmail.openSentEmail()

    const actualResults = [
      await gmail.getSentEmailRecipient(),
      await gmail.getSentEmailSubject(),
      await gmail.getSentEmailMessage(),
      await gmail.getMessageSentTime()
    ]
    const expectedResults = [
      mail.recipient,
      mail.subject,
      mail.message,
      '0 minutes ago'
    ]

    await assert.deepEquals(actualResults, expectedResults)
  })
})



