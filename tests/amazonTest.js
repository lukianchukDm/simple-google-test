const Assert = require('../helpers/assert')
const Page   = require('../pageObjects/page')

const google     = 'https://google.com'
const searchText = 'amazon'

describe('Searching "amazon" in google', () => {
  let driver
  let page
  let assert

  before(async() => {
    driver = require('../index').driver
    page   = new Page(driver)
    assert = new Assert(driver)
  })

  beforeEach(async() => {
    await page.navigateTo(google)

    await page.googleHome.search(searchText)
  })

  it('Should search "amazon" on Google', async() => {
    const googleResults = page.googleResults

    const expectedLink = 'https://www.amazon.com'

    await googleResults.getSearchResults()
      .then(async([ result ]) => await googleResults.getLink(result))
      .then(async(link) => await assert.equals(link, expectedLink))
  })

  it('Should count results on a page', async() => {
    await page.googleResults.getSearchResults()
      .then(results => results.length)
      .then(async(length) => await console.log(`There are ${length} results on a page`)) //eslint-disable-line
  })

  it('Should get the target page title', async() => {
    const expectedTitle = 'Amazon.com: Online Shopping for Electronics, Apparel, Computers, Books, DVDs & more'

    await page.googleResults.navigateToResult(0)
      .then(() => page.title)
      .then(async(title) => await assert.equals(expectedTitle, title))
  })
})
