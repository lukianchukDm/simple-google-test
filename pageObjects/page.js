const GoogleHome    = require('./googleHome')
const GoogleResults = require('./googleResults')
const GmailLogin    = require('./gmailLogin')
const Gmail         = require('./gmail')

class Page {
  constructor(driver) {
    this.driver = driver

    this.googleHome    = new GoogleHome(this.driver)
    this.googleResults = new GoogleResults(this.driver)
    this.gmailLogin    = new GmailLogin(this.driver)
    this.gmail         = new Gmail(this.driver)
  }

  get title() {
    return this.driver.getTitle()
  }

  async wait() {
    this.driver.wait(() => {
      return this.driver.executeScript('return document.readyState').then(readyState => {
        return readyState === 'complete'
      })
    })
  }

  async navigateTo(url) {
    await this.driver.get(url)

    await this.wait
  }
}

module.exports = Page
