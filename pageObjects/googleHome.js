const waitForElement = require('../helpers/waitForElement')
const { By }   = require('selenium-webdriver')
const { wait } = require('./page')
const Input    = require('../elements/input')

const selectors = {
  searchInput:  By.css('input.gLFyf'),
  searchButton: By.css('input[type = "submit"]')
}

class GoogleHome {
  constructor(driver) {
    this.driver = driver

    this.searchInput = new Input(this.driver, selectors.searchInput)
  }

  get searchButton() {
    return this.driver.findElement(selectors.searchButton)
  }

  async fillInput(text) {
    await this.searchInput.fill(text)
  }

  async clickSearchButton() {
    await waitForElement(this.driver, selectors.searchButton)
    await this.searchButton.click()

    await wait
  }

  async search(text) {
    await this.searchInput.fill(text)
    await this.searchInput.pressEnter()

    await wait
  }
}

module.exports = GoogleHome
