const { By } = require('selenium-webdriver')
const Input  = require('../elements/input')
const waitForElement = require('../helpers/waitForElement')

const selectors = {
  login:      By.css('input[type = "email"]'),
  password:   By.css('input[type = "password"]'),
  nextButton: By.css('[id*="Next"]'),
  menu:       By.css('[aria-label="Main menu"]'),
}

class GmailLogin {
  constructor(driver) {
    this.driver = driver

    this.loginInput    = new Input(this.driver, selectors.login)
    this.passwordInput = new Input(this.driver, selectors.password)
  }

  get nextButton() {
    return this.driver.findElement(selectors.nextButton)
  }

  async signIn({ login, password }) {
    await this.loginInput.fill(login)
    await this.nextButton.click()
    await waitForElement(this.driver, selectors.password)

    await this.passwordInput.fill(password)
    await this.nextButton.click()
    await waitForElement(this.driver, selectors.menu)
  }
}

module.exports = GmailLogin
