const { By }   = require('selenium-webdriver')
const { wait } = require('./page')

const selectors = {
  result: By.css('cite')
}

class GoogleResults {
  constructor(driver) {
    this.driver = driver
  }

  async getSearchResults() {
    return this.driver.findElements(selectors.result)
  }

  async getLink(element) {
    return element.getText().then(text => text.split('›')[0].trim())
  }

  async filterResults(results) {
    let links = []

    for (const result of results) {
      const isNotPeopleAlsoAskBlock = await this._isNotPeopleAlsoAskBlock(result)
      const isNotAd = await this._isNotAd(result)

      isNotPeopleAlsoAskBlock && isNotAd ? links.push(await this.getLink(result)) : result
    }

    return links
  }

  async navigateToResult(index) {
    await this.getSearchResults()
      .then(async(results) => await results[index].click())

    await wait
  }

  async _isNotPeopleAlsoAskBlock(result) {
    return this.getLink(result).then(link => !!link)
  }

  async _isNotAd(result) {
    return this.driver.executeScript('return arguments[0].previousSibling', result)
      .then(element => element == null)
  }
}

module.exports = GoogleResults
