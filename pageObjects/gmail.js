const { By } = require('selenium-webdriver')
const Input  = require('../elements/input')
const waitForElement = require('../helpers/waitForElement')

const selectors = {
  composeButton:       By.css('.z0 div'),
  recipientInput:      By.css('[name="to"]'),
  subjectInput:        By.css('[name="subjectbox"]'),
  textInput:           By.css('.editable'),
  sendButton:          By.css('[data-tooltip*="Send"]'),
  sentLetterRecipient: By.css('[role="main"] [name="dmitriyl"]'),
  sentLetterSubject:   By.css('[role="main"] h2'),
  sentLetterMessage:   By.css('[role="main"] .a3s > div'),
  messageSentLink:     By.css('#link_vsm'),
  messageSentTime:     By.css('[role="main"] .g3')
}

class Gmail {
  constructor(driver) {
    this.driver = driver

    this.recipientInput = new Input(this.driver, selectors.recipientInput)
    this.subjectInput   = new Input(this.driver, selectors.subjectInput)
    this.textInput      = new Input(this.driver, selectors.textInput)
  }

  get composeButton() {
    return this.driver.findElement(selectors.composeButton)
  }

  get sendButton() {
    return this.driver.findElement(selectors.sendButton)
  }

  get sentLettersButton() {
    return this.driver.findElement(selectors.sentLettersButton)
  }

  get sentLetterRecipient() {
    return this.driver.findElement(selectors.sentLetterRecipient)
  }

  get sentLetterSubject() {
    return this.driver.findElement(selectors.sentLetterSubject)
  }

  get sentLetterMessage() {
    return this.driver.findElement(selectors.sentLetterMessage)
  }

  get messageSentLink() {
    return this.driver.findElement(selectors.messageSentLink)
  }

  get messageSentTime() {
    return this.driver.findElement(selectors.messageSentTime)
  }

  async sendEmail(mail) {
    await this.openNewMessage()
    await this.fillEmail(mail)
    await this.send()
  }

  async openNewMessage() {
    await this.composeButton.click()

    await waitForElement(this.driver, selectors.sendButton)
  }

  async fillEmail(data) {
    const { recipient, subject, message } = data

    await this.recipientInput.fill(recipient)
    await this.subjectInput.fill(subject)
    await this.textInput.fill(message)
  }

  async send() {
    await this.sendButton.click()

    await waitForElement(this.driver, selectors.messageSentLink)
  }

  async openSentEmail() {
    await this.messageSentLink.click()

    await waitForElement(this.driver, selectors.sentLetterRecipient)
  }

  async getSentEmailRecipient() {
    return this.sentLetterRecipient.getAttribute('email')
  }

  async getSentEmailSubject() {
    return this.sentLetterSubject.getText()
  }

  async getSentEmailMessage() {
    return this.sentLetterMessage.getText()
  }

  async getMessageSentTime() {
    return this.messageSentTime.getText()
      .then(text => text.split('(')[1].slice(0, -1))
  }
}

module.exports = Gmail
