# Create test and run it locally

At this point we don't need a remote driver, just building local webdriver, it starts and runs installed browser.

Few techniques to follow while writing a test suite:
1. To describe page elements and methods of working with them, use Page Object test model.
2. Keep common elements in separate page object and reuse them instead of duplicating.
3.

# Set the environment

How to build the most powerfull `Terminal` on macOS:
```bash
https://medium.com/ayuth/iterm2-zsh-oh-my-zsh-the-most-power-full-of-terminal-on-macos-bdb2823fb04c
```

How to use `Terminal` on macOS:
```bash
https://macpaw.com/how-to/use-terminal-on-mac
```

Download and install `Node.js` - is a JavaScript server environment and `npm` - is a world's largest software library (registry) and a software Package Manager and Installer:
```bash
https://nodejs.org/en/
```

Install `mocha` - is a JavaScript test framework (install is global, execute command in terminal from any directory):
```bash
$ npm install --global mocha
```

At this point we gonna need to create a test directory. Don't worry about the structure, just come up with a simple name and location.

Install `selenium-webdriver` - is a browser automation library (execute from a test directory):
```bash
$ npm install selenium-webdriver
```

Install `chromedriver` - is a tool for automated testing of webapps. It provides capabilities for navigating to web pages, user input, JavaScript execution, and more (execute from a test directory):
```bash
$ npm install chromedriver
```

Install `eslint` - is a pluggable linting utility for JavaScript (execute from a test directory):
```bash
$ npm install eslint
```



test
package.json
  run locally

dockerfile - is a text document that contains all the commands a user could call on the command line to assemble an image
docker-compose.yml - is a config file for docker-compose

Docker Compose is a tool for defining and running multi-container Docker applications

package.json
  run in docker

gitlab-ci.yml
  run in gitlab

eslint
gitignore
health



SELENOID

mkdir /Users/salvatore/Desktop/QA/simple-google-test/selenoid

docker run --rm -v /var/run/docker.sock:/var/run/docker.sock aerokube/cm:1.0.0 selenoid \
  --last-versions 2 --tmpfs 128 --pull > /Users/salvatore/Desktop/QA/simple-google-test/selenoid/browsers.json

docker run -d --name selenoid -p 4444:4444 -v /Users/salvatore/Desktop/QA/simple-google-test/selenoid:/etc/selenoid:ro \
      -v /var/run/docker.sock:/var/run/docker.sock aerokube/selenoid:1.1.1


ALLURE

https://github.com/allure-framework/allure-mocha
https://github.com/allure-examples/allure-mocha-example/blob/master/package.json




This set of tests runs in a browser with `testcafe`.

Options to run tests:

1. **Local** backend
    * start mongo, redis and haproxy in `he` project:
      ```bash
      $ cd he
      $ npm run pm2support
      ```
    * start environment (requires root password for starting react app):
      ```bash
      $ cd ../he-react
      $ npm i
      $ npm run start-env-local
      ```


 docker system prune --volumes
 docker images
 docker rmi IMAGE_ID


 health-check before mocha

